import React, { useState } from "react";
import { View, Text, StyleSheet } from "react-native";
import { Ionicons } from "@expo/vector-icons";

export default () => {
  const [firstName, setFirstName] = useState("Eugen");

  return (
    <View style={styles.container}>
      <View style={styles.greetingContainer}>
        <Ionicons name="ios-body" size={60} />
        <Text style={styles.greetingText}>Good morning, {firstName}</Text>
      </View>

      <View style={styles.card}>
        <Text>No symptoms today</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF",
  },
  greetingContainer: {
    margin: 20,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  greetingText: {
    fontSize: 20,
    marginLeft: 20,
  },
  card: {
    borderRadius: 10,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.3,
    shadowRadius: 2.0,
    elevation: 2,
    padding: 20,
    margin: 20,
    backgroundColor: "#EEEEEE",
  },
});
