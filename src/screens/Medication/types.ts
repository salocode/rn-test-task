export interface Medication {
  id: string;
  name: string;
}

export interface MedicationItem {
  id: string;
  medication: Medication;
  wasTaken: boolean;
  updatedAt?: number;
}
