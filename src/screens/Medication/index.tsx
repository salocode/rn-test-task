import React, { useCallback, useEffect, useState } from "react";
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  ActivityIndicator,
} from "react-native";
import CheckBox from "@react-native-community/checkbox";
import Touchable from "react-native-touchable-safe";

import { MedicationItem } from "./types";
import { getMedications, saveMedications, formatTimestamp } from "./functions";

export default ({ navigation }: { navigation: any }) => {
  const [items, setItems] = useState<MedicationItem[]>([]);

  useEffect(() => {
    getMedications().then((items) => setItems(items));
  }, []);

  const renderItem = useCallback(
    ({ item }: { item: MedicationItem }) => {
      const { medication, updatedAt, wasTaken } = item;

      const onValueChange = (newValue: boolean) => {
        const newItems = [...items];

        const newItem = newItems.find(
          (i) => i.id === item.id
        ) as MedicationItem;
        newItem.wasTaken = newValue;
        newItem.updatedAt = newValue ? new Date().getTime() : undefined;

        setItems(newItems);
        saveMedications(newItems);
      };

      const goToDetails = () => {
        navigation.navigate("Medication Details", {
          medication: item,
        });
      };

      return (
        <Touchable onPress={goToDetails}>
          <View style={styles.listItem}>
            <Text style={styles.itemTime}>{formatTimestamp(updatedAt)}</Text>
            <Text style={styles.itemName}>{medication.name}</Text>
            <CheckBox value={wasTaken} onValueChange={onValueChange} />
          </View>
        </Touchable>
      );
    },
    [navigation, items]
  );

  if (!items) {
    return (
      <View style={styles.loaderContainer}>
        <ActivityIndicator size="large" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <FlatList
        data={items}
        style={styles.list}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  loaderContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  container: {
    flex: 1,
    backgroundColor: "#FFF",
  },
  list: {
    flex: 1,
  },
  listItem: {
    flexDirection: "row",
    flex: 1,
    alignItems: "center",
    padding: 20,
  },
  itemTime: {
    marginLeft: 10,
    fontSize: 20,
  },
  itemName: {
    flex: 1,
    fontSize: 20,
    marginLeft: 20,
    marginRight: 20,
  },
});
