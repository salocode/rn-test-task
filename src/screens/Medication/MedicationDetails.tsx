import React, { useCallback } from "react";
import { View, Text, StyleSheet, ActivityIndicator } from "react-native";
import { formatTimestamp } from "./functions";
import Button from "~/components/Button";

interface Props {
  navigation: any;
  route: any;
}

export default ({ navigation, route }: Props) => {
  const data = route.params.medication;

  const goBack = useCallback(() => {
    navigation.goBack();
  }, [navigation]);

  if (!data) {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" />
      </View>
    );
  }

  const { updatedAt, medication } = data;

  return (
    <View style={styles.container}>
      <Text style={styles.text}>{medication.name}</Text>
      <Text style={styles.text}>{formatTimestamp(updatedAt)}</Text>

      <Button caption="GO BACK" onPress={goBack} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF",
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    fontSize: 20,
    margin: 10,
  },
});
