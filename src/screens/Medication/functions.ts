import AsyncStorage from "@react-native-community/async-storage";
import moment from "moment";
import { MedicationItem } from "./types";

const MEDICATIONS = [
  {
    id: "1",
    name: "Analgin",
  },
  {
    id: "2",
    name: "Aspirin",
  },
];

const ITEMS = MEDICATIONS.map((medication, index) => {
  return {
    id: `${medication.id}_${index}`,
    medication,
    wasTaken: false,
  };
});

const STORAGE_KEY = "storage_medications";

export const getMedications = async (): Promise<MedicationItem[]> => {
  try {
    const serialized = await AsyncStorage.getItem(STORAGE_KEY);
    if (serialized) {
      return JSON.parse(serialized);
    } else {
      return ITEMS;
    }
  } catch (error) {
    console.log(error);
    return [];
  }
};

export const saveMedications = async (items: MedicationItem[]) => {
  try {
    const serialized = JSON.stringify(items);
    await AsyncStorage.setItem(STORAGE_KEY, serialized);
  } catch (error) {
    console.log(error);
  }
};

export const formatTimestamp = (timestamp: number | undefined) => {
  return timestamp ? moment(timestamp).format("HH:mm") : "--:--";
};
