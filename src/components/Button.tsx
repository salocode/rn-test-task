import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Touchable from "react-native-touchable-safe";

export interface Props {
  caption: string;
  onPress: () => void;
  disabled?: boolean;
}

export default ({ caption, onPress, disabled = false }: Props) => {
  return (
    <Touchable onPress={onPress} disabled={disabled}>
      <View style={styles.button}>
        <Text style={styles.buttonText}>{caption}</Text>
      </View>
    </Touchable>
  );
};

const styles = StyleSheet.create({
  button: {
    margin: 5,
    justifyContent: "center",
    borderRadius: 10,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,
    elevation: 1,
    backgroundColor: "#BBEEBB",
  },
  buttonText: {
    marginTop: 10,
    marginRight: 20,
    marginBottom: 10,
    marginLeft: 20,
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
    color: "#77AA77",
  },
});
