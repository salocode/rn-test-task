import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";

export default class extends Component {
  constructor(props: any) {
    super(props);
    this.state = { error: null };
  }

  static getDerivedStateFromError(error: Error) {
    return { error: error.message };
  }

  componentDidCatch(error: Error) {
    console.error(error);
  }

  render() {
    const { error } = this.state as any;

    if (error) {
      return (
        <View style={styles.container}>
          <Text style={styles.message}>{error}</Text>
        </View>
      );
    }

    return this.props.children;
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 40,
    backgroundColor: "#FF8888",
    justifyContent: "center",
    textAlign: "center",
  },
  message: {
    fontSize: 20,
    color: "#FFF",
    fontWeight: "bold",
  },
});
