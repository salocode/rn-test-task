import * as React from "react";
import { ColorSchemeName } from "react-native";
import {
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
} from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import NotFoundScreen from "../screens/NotFoundScreen";
import BottomTabNavigator from "./BottomTabNavigator";
import LinkingConfiguration from "./LinkingConfiguration";
import MedicationDetails from "~/screens/Medication/MedicationDetails";
import { useSerializedState, useStateChangeCallback } from "./hooks";

const { Navigator, Screen } = createStackNavigator();
const NO_HEADER = { headerShown: false };

interface Props {
  colorScheme: ColorSchemeName;
}

export default function Navigation({ colorScheme }: Props) {
  const [isReady, initialState] = useSerializedState();
  const onStateChange = useStateChangeCallback();

  if (!isReady) {
    // show some loading (splash) screen here
    return null;
  }

  return (
    <NavigationContainer
      initialState={initialState as any}
      onStateChange={onStateChange}
      linking={LinkingConfiguration}
      theme={colorScheme === "dark" ? DarkTheme : DefaultTheme}
    >
      <Navigator mode="modal">
        <Screen
          name="Root"
          component={BottomTabNavigator}
          options={NO_HEADER}
        />
        <Screen
          name="NotFound"
          component={NotFoundScreen}
          options={NO_HEADER}
        />
        <Screen name="Medication Details" component={MedicationDetails} />
      </Navigator>
    </NavigationContainer>
  );
}
