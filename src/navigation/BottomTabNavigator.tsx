import React, { useMemo } from "react";
import { Ionicons } from "@expo/vector-icons";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import Colors from "~/constants/Colors";
import useColorScheme from "~/hooks/useColorScheme";
import DashboardNavigator from "./tabs/DashboardNavigator";
import MedicationNavigator from "./tabs/MedicationNavigator";
import CalendarNavigator from "./tabs/CalendarNavigator";

type BottomTabParamList = {
  Dashboard: undefined;
  Medication: undefined;
  Calendar: undefined;
};

function createTabOptions(iconName: string) {
  return {
    tabBarIcon: ({ color }: { color: string }) => (
      <Ionicons size={30} name={iconName} color={color} />
    ),
  };
}

const { Navigator, Screen } = createBottomTabNavigator<BottomTabParamList>();
const DASHBOARD = createTabOptions("ios-home");
const MEDICATION = createTabOptions("ios-medical");
const CALENDAR = createTabOptions("ios-calendar");

export default function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  const tabBarOptions = useMemo(
    () => ({
      activeTintColor: Colors[colorScheme].tint,
    }),
    [colorScheme]
  );

  return (
    <Navigator initialRouteName="Dashboard" tabBarOptions={tabBarOptions}>
      <Screen
        name="Dashboard"
        component={DashboardNavigator}
        options={DASHBOARD}
      />
      <Screen
        name="Medication"
        component={MedicationNavigator}
        options={MEDICATION}
      />
      <Screen
        name="Calendar"
        component={CalendarNavigator}
        options={CALENDAR}
      />
    </Navigator>
  );
}
