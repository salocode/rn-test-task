import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Calendar from "~/screens/Calendar";

const { Navigator, Screen } = createStackNavigator();
const OPTIONS = { headerTitle: "Calendar" };

export default () => {
  return (
    <Navigator>
      <Screen name="Calendar" component={Calendar} options={OPTIONS} />
    </Navigator>
  );
};
