import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Medication from "~/screens/Medication";

const { Navigator, Screen } = createStackNavigator();
const OPTIONS = { headerTitle: "Medication" };

export default () => {
  return (
    <Navigator>
      <Screen name="Medication" component={Medication} options={OPTIONS} />
    </Navigator>
  );
};
