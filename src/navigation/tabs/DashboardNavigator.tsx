import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Dashboard from "~/screens/Dashboard";

const { Navigator, Screen } = createStackNavigator();
const OPTIONS = { headerTitle: "Dashboard" };

export default () => {
  return (
    <Navigator>
      <Screen name="Dashboard" component={Dashboard} options={OPTIONS} />
    </Navigator>
  );
};
