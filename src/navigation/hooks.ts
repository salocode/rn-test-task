import { useCallback, useEffect, useState } from "react";
import { AsyncStorage, Linking } from "react-native";

const PERSISTENCE_KEY = "NAVIGATION_STATE";

export const useSerializedState = () => {
  const [isReady, setIsReady] = useState(false);
  const [initialState, setInitialState] = useState();

  useEffect(() => {
    if (isReady) {
      return;
    }

    const restoreState = async () => {
      try {
        // check intial URL get params for deep linking support
        const initialUrl = await Linking.getInitialURL();
        console.log(`initial url: ${initialUrl}`);

        // Only restore state if there's no deep link
        const savedStateString = await AsyncStorage.getItem(PERSISTENCE_KEY);
        const state = savedStateString ? JSON.parse(savedStateString) : null;
        if (state) {
          setInitialState(state);
        }
      } catch (error) {
        console.log(error);
      } finally {
        setIsReady(true);
      }
    };
    restoreState();
  }, [isReady]);

  return [isReady, initialState];
};

export const useStateChangeCallback = () => {
  return useCallback((state) => {
    try {
      AsyncStorage.setItem(PERSISTENCE_KEY, JSON.stringify(state));
    } catch (error) {
      console.log(error);
      console.log(
        "Note that you should use only JSON serializable props as route params!"
      );
    }
  }, []);
};
